/* You DID it!
 * 
 * This file is part of You DID it!
 * 
 * You DID it! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You DID it! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * You DID it! uses jQuery, licensed by the jQuery Fundation
 * under the MIT License (see https://jquery.org/license/)
 * and Mustache.js, licensed under the MIT License.
 */
 
// This file contains game state management functions.
// Prefix: stt

/** Trigger things on state entering.
 * @param state State entered in. */
function stt_onStateEnter(state) {
	// Start state timers
	if ('timers' in state) {
		var timers = state['timers'];
		if (!Array.isArray(timers)) {
			timers = [timers];
		}
		for (var i = 0; i < timers.length; i++) {
			var timer = timers[i];
			var t = eng_declareTimer(tmr_initTimer("_stateTimer-" + i,
					timer["time"], timer["result"]));
			tmr_startStateTimer(t);
		}
	}
}
