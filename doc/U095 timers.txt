Title: Setting timers

You can simulate a timer that will be started when entering a state. The time will reduce after each action and if it reaches 0, a result is triggered. If the player leaves the state, the timer stops.

To add a timer to a state add the following object in you state

> "timers": {"time": X, "result":{result object}}

You can set an array of timers if you wish.

Note that the action result is triggered before the timer decreases. That is if the last action before the timer reaches 0 is moving out of the way, the timer is stopped before it's action is triggered.