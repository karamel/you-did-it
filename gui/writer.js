/* You DID it!
 * 
 * This file is part of You DID it!
 * 
 * You DID it! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You DID it! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * You DID it! uses jQuery, licensed by the jQuery Fundation
 * under the MIT License (see https://jquery.org/license/)
 * and Mustache.js, licensed under the MIT License.
 */
 
// This file contains progressive text displayer

// prefix: wrt

/** Default number of characters written in one minute */
var WRT_DEFAULT_SPEED = 300;
/** Pause in seconds between two paragraphs */
var WRT_DEFAULT_PARAGRAPH_PAUSE = 0.5;

var _wrt_symbols = {};

function wrt_init(containerElem, text, options) {
	switch (arguments.length) {
	case 2:
		options = {};
	}
	if (!Array.isArray(text)) {
		text = Array(text);
	}
	var speed = WRT_DEFAULT_SPEED;
	var pause = WRT_DEFAULT_PARAGRAPH_PAUSE;
	var color = null;
	var name = null;
	if ("speed" in options) {
		speed = options["speed"];
	}
	if ("pause" in options) {
		pause = options["pause"];
	}
	if ("color" in options) {
		color = options["color"];
	}
	if ("name" in options) {
		name = options["name"];
	}
	var writer = {"container": containerElem,
		"text": text,
		"speed": speed,
		"pause": pause,
		"color": color,
		"name": name,
		"paragraphIndex": 0,
		"charIndex": -1,
		"timeoutId": null,
	};
	return writer;
}

/** Start displaying text progressively. Once started, it should be
 * stopped when interrupted. */
function wrt_start(writer) {
	var id;
	do {
		id = new Date().getTime() * 10 + (Math.floor(Math.random()*10));
	} while (!id in _wrt_symbols);
	_wrt_symbols[id] = writer;
	writer['container'].innerHTML = '';
	writer['container'].setAttribute('onclick', "javascript:wrt_clicked('" + id + "');");
	if (!writer['ready']) {
		_wrt_prepare(writer);
	}
	_wrt_writeStep(writer);
}

/** Display everything from the writer instantly.
 * The writer must be running */
function wrt_stop(writer) {
	if (!wrt_isRunning(writer)) {
		return;
	}
	for (var i = 0; i < writer['text'].length; i++) {
		var pElem = writer['container'].getElementsByClassName('writer-text')[i];
		var visibleTxtElem = pElem.childNodes[0];
		var hiddenTxtElem = pElem.childNodes[1];
		visibleTxtElem.innerHTML = writer['text'][i];
		hiddenTxtElem.innerHTML = '';
	}
	writer["charIndex"] = -1;
	writer["paragraphIndex"] = writer['text'].length;
	_wrt_close(writer);
}

function wrt_isRunning(writer) {
	return writer["timeoutId"] != null;
}

function wrt_clicked(id) {
	if (id in _wrt_symbols) {
		wrt_stop(_wrt_symbols[id]);
	} else {
		console.warn('Unknown writer ' + id);
	}
}

/** Prepare the text to be displayed */
function _wrt_prepare(writer) {
	// Write the full text and hide it.
	var color = null;
	if (writer["color"]) {
		color = 'color:#' + escAttr(writer['color']);
	}
	if (writer["name"]) {
		var nameElem = document.createElement('p');
		nameElem.setAttribute('class', 'writer-name');
		if (color != null) { nameElem.setAttribute('style', color); }
		nameElem.innerHTML = escHtml(writer['name']);
		writer["container"].appendChild(nameElem);
	}
	for (var i = 0; i < writer["text"].length; i++) {
		var pElem = document.createElement('p');
		pElem.setAttribute('class', 'writer-text');
		if (color != null) { pElem.setAttribute('style', color); }
		visibleTxtElem = document.createElement('span');
		hiddenTxtElem = document.createElement('span');
		hiddenTxtElem.innerHTML = escHtml(writer['text'][i]);
		hiddenTxtElem.classList.add("invisible");
		pElem.appendChild(visibleTxtElem);
		pElem.appendChild(hiddenTxtElem);
		writer["container"].appendChild(pElem);
	}
}

/** Clean writer data once it has finished. */
function _wrt_close(writer) {
	if (writer['timeoutId'] != null) {
		clearTimeout(writer['timeoutId']);
		writer["timeoutId"] = null;
	}
	writer['container'].removeAttribute('onclick');
	delete _wrt_symbols[writer.id];
}

function _wrt_writeStep(writer) {
	if (writer["paragraphIndex"] == writer["text"].length) {
		// End
		_wrt_close(writer);
		return;
	}
	if (writer["charIndex"] == -1) {
		// New paragraph
		writer["charIndex"]++;
		if (writer["paragraphIndex"] != 0) {
			// Pause before starting the next paragraph
			writer["timeoutId"] = setTimeout(function() { _wrt_writeStep(writer); }, writer["pause"] * 1000);
			return;
		}
		_wrt_writeStep(writer);
		return;
	}
	var paragraph = writer["text"][writer["paragraphIndex"]];
	if (writer["charIndex"] < paragraph.length) {
		// In paragraph, get one character out of the invisible span
		var currentP = writer['container'].getElementsByClassName('writer-text')[writer['paragraphIndex']];
		var visibleTxtElem = currentP.childNodes[0];
		var hiddenTxtElem = currentP.childNodes[1];
		visibleTxtElem.innerHTML += paragraph[writer['charIndex']];
		hiddenTxtElem.innerHTML = hiddenTxtElem.innerHTML.substring(1);
		writer["charIndex"]++;
		if (writer["charIndex"] != paragraph.lengh) {
			// Pause before the next char
			writer["timeoutId"] = setTimeout(function() { _wrt_writeStep(writer); }, 6000.0 / writer["speed"]);
			return;
		}
		_wrt_writeStep(writer);
		return;
	}
	// End of paragraph
	writer["charIndex"] = -1;
	writer["paragraphIndex"]++;
	_wrt_writeStep(writer);
	return;
}