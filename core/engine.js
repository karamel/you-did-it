/* You DID it!
 * 
 * This file is part of You DID it!
 * 
 * You DID it! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You DID it! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * You DID it! uses jQuery, licensed by the jQuery Fundation
 * under the MIT License (see https://jquery.org/license/)
 * and Mustache.js, licensed under the MIT License.
 */

// This is the main controller for You DID it!,
// the one that connects everything
// Prefix: eng

var ENG_I18N_DEFAULT_PRIORITY = 10;
var ENG_I18N_REQ_PRIORITY = 100;

// Game variables
/////////////////
/** Loaded game data.
 * 'hintThresholds' (array of int) Number of bad actions to suggest
 * 'stateNames' (associative array) dict of state codes and index.
 * before showing hints. Must be in ascending ordered. */
var _eng_gameData = {
	'title': null,
	'footer': null,
	'hintThresholds': [5, 10, 15, 20, 25],
	'stateCodes': {},
	'i18nData': {},
	'characters': [],
	'global_actions': [],
}

var _eng_loadingStatus = {
	'gameLoaded': false,
	'defaultResLangLoaded': false,
	'defaultGameLangLoaded': false,
	'resLangLoaded': false,
	'gameLangLoaded': false,
}

function eng_getGameCode() {
	return _eng_gameData['game']; // Passed by value, read only :)
}

function eng_getGameTranslations() {
	return _eng_gameData['i18nData'];
}

function eng_getCharacters() {
	return _eng_gameData["characters"];
}
function eng_getCharacter(code) {
	for (var i = 0; i < _eng_gameData["characters"].length; i++) {
		var char = _eng_gameData["characters"][i];
		if (char["code"] == code) {
			return char;
		}
	}
	return null;
}

/** In memory game state data.
 * 'badActions' (int) Counts the number of useless actions to display hints.
 * 'hintsCount' (int) Number of unlocked hints. */
var _eng_gameState = {};

function _eng_initGameState() {
	_eng_gameState = {
		"currentStateIndex": -1,
		"badActions": 0,
		"hintsCount": 0,
		"timers": [],
	};
}

function eng_getCurrentState() {
	if (_eng_gameData['currentStateIndex'] == -1) {
		return null;
	}
	return _eng_gameData['states'][_eng_gameState['currentStateIndex']];
}

// Actions management
////////////////////

var _eng_actionObservers = [];
/** Add a listener that will be called when action picking changes.
 * @param callback (function) Function to be called. No Argument given,
 * use act_ functions to check state. */
function eng_observeActionChange(callback) {
	_eng_actionObservers.push(callback);
	callback();
}
function eng_unobserveActionChange(callback) {
	_eng_actionsObservers.remove(callback);
}
function _eng_notifyActionChangeObservers() {
	for (var i = 0; i < _eng_actionObservers.length; i++) {
		_eng_actionObservers[i]();
	}
}

function _eng_checkActionTriggering() {
	// Check for an action in the current state
	var actionTriggered = act_checkActionTriggering(eng_getCurrentState()['actions']);
	if (actionTriggered == null || act_isDefaultResult(actionTriggered)) {
		// Check for an action in global actions if not found
		actionTriggered = act_checkActionTriggering(_eng_gameData["global_actions"]);
	}
	if (actionTriggered != null) {
		// Action is matching, trigger it
		_eng_proceedAction(actionTriggered);
		act_resetState();
	}
}

function eng_actionPicked(actionCode) {
	var action = null;
	// Get action object from action code
	for (var i = 0; i < _eng_gameData['actions'].length; i++) {
		var actionIter = _eng_gameData['actions'][i];
		if (actionIter['code'] == actionCode) {
			action = actionIter;
			break;
		}
	}
	if (action == null) {
		console.error("Picked an undefined action");
		return;
	}
	// Update the automat, trigger a result if it can
	if (act_actionPicked(action)) {
		_eng_checkActionTriggering();
		_eng_notifyActionChangeObservers();
	}
}

function eng_targetPicked(item) {
	// Check in the action automat is expecting a target and pass it
	if (act_getActionState() == ACT_PICK_TARGET) {
		act_targetPicked(item);
		_eng_checkActionTriggering(eng_getCurrentState()['actions']);
		_eng_notifyActionChangeObservers();
	} else {
		// TODO: we could do something for the default action (observe?)
	}
}
function eng_inventoryPicked(location) {
	eng_targetPicked(itm_getInventoryItem(_eng_gameState['inventory'], location));
}

function eng_mapPicked(mapIndex) {
	var mapItem = eng_getCurrentState()["map-items"][mapIndex];
	if ("item" in mapItem) {
		var item = mapItem["item"];
		var desc = mapItem["desc"];
		if (!itm_isDiscovered(_eng_gameState["inventory"], item)) {
			// Discover the item and do nothing with it.
			_eng_discoverItem(item);
			gme_showResult(desc, item);
		} else {
			switch (act_getActionState()) {
			case ACT_PICK_ACTION:
				// Observe (show description)
				gme_showResult(desc, item);
				break;
			case ACT_PICK_TARGET:
				// Pick the item as target of the current action
				eng_targetPicked(item);
				break;
			}
		}
	}
	if (("result" in mapItem)) {
		rsl_proceedResult(mapItem["result"], RSL_SRC_MAP);
	}
}

function _eng_discoverItem(item) {
	if (itm_discoverItem(_eng_gameState["inventory"], item)) {
		_eng_notifyObjectsChangeObservers(ENG_OBJECT_DISCOVERED, item);
	}
}
// Result parsing
/////////////////

function _eng_proceedAction(action) {
	var didSomething = false;
	// Check if conditions are met or 'else' is present
	if ('if' in action) {
		if (cnd_processIf(action['if'], _eng_gameState)) {
			didSomething = rsl_proceedResult(action['result'], RSL_SRC_ACTION);
		} else {
			// If there's an else pick it
			if ('else' in action) {
				didSomething = rsl_proceedResult(action['else'], RSL_SRC_ACTION);
			} else {
				// Condition unmet without else, trigger default result
				// TODO: unelegant 'if' failed action switching
				didSomething = rsl_proceedResult(RSL_DONT_DO_THAT, RSL_SRC_ACTION);
			}
		}
	} else {
		// Regular action without condition
		didSomething = rsl_proceedResult(action['result'], RSL_SRC_ACTION);
	}
	// Check if the game progressed
	if (!didSomething) {
		if ('hint_count' in action) {
			eng_badAction(action['hint_count']);
		} else {
			eng_badAction();
		}
	}
}

// Hints management
///////////////////

var _eng_hintObservers = [];

/** Add a listener that will be called when hint count changes.
 * @param callback (function) Function to be called. Arguments are the
 * old number of hints unlocked and the new number. */
function eng_observeHintChange(callback) {
	_eng_hintObservers.push(callback);
	var hints = [];
	if ('hints' in eng_getCurrentState()) {
		hints = eng_getCurrentState()['hints'];
	}
	callback(0, _eng_gameState['hintsCount'], hints);
}
function eng_unobserveHintChange(callback) {
	_eng_hintObservers.remove(callback);
}

/** Notify hint observers that it has changed, passing them old count
 * and the actual count. */
function _eng_notifyHintChangeObservers(oldCount) {
	var hints = [];
	if ('hints' in eng_getCurrentState()) {
		hints = eng_getCurrentState()['hints'];
	}
	for (var i = 0; i < _eng_hintObservers.length; i++) {
		_eng_hintObservers[i](oldCount, _eng_gameState['hintsCount'], hints);
	}
}

function _eng_updateHints() {
	var hintsCount = 0;
	// Check if a threshold is reached (or even multiple)
	while (_eng_gameData['hintThresholds'].length > hintsCount
			&& _eng_gameData['hintThresholds'][hintsCount] <= _eng_gameState['badActions']) {
		hintsCount++;
	}
	if (hintsCount != _eng_gameState['hintsCount']) {
		var oldCount = _eng_gameState['hintsCount'];
		_eng_gameState['hintsCount'] = hintsCount;
		_eng_notifyHintChangeObservers(oldCount);
	}
}

/** Incremend the bad action count and check
 * if a hint should be shown.
 * @param hintCount (optional integer) increment the action count
 * to show hints. Default 1. */
function eng_badAction(hintCount) {
	switch (arguments.length) {
	case 0:
		hintCount = 1;
	}
	_eng_gameState['badActions'] += hintCount;
	_eng_updateHints();
}

// State management
///////////////////

var _eng_stateObservers = [];
/** Add a listener that will be called when state changes.
 * @param callback (function) Function to be called. Arguments is the
 * new state. */
function eng_observeStateChange(callback) {
	_eng_stateObservers.push(callback);
	callback(eng_getCurrentState());
}
function eng_unobserveStateChange(callback) {
	_eng_stateObservers.remove(callback);
}
function _eng_notifyStateChangeObservers() {
	for (var i = 0; i < _eng_stateObservers.length; i++) {
		_eng_stateObservers[i](eng_getCurrentState());
	}
}

/** Move the game to the given state, by index. */
function _eng_switchState(stateIndex) {
	// Reset bad action count and hints
	_eng_gameState['badActions'] = 0;
	_eng_updateHints();
	// Reset action state
	act_resetState();
	// Effectively switch to the new state
	_eng_gameState['currentStateIndex'] = stateIndex;
	var state = _eng_gameData['states'][stateIndex];
	// Auto-save (except on endings or explicitely disabled)
	if (state["type"] != "end"
			&& (!(('autosave' in state) && (state['autosave'] === false)))) {
		svg_saveGame(eng_getGameCode(), SVG_AUTOSAVE_NAME, _eng_gameState);
	}
	// Enter state actions
	stt_onStateEnter(state);
	// Notify
	_eng_notifyStateChangeObservers();
}

/** Move the game to next state. */
function eng_nextState() {
	_eng_switchState(_eng_gameState['currentStateIndex'] + 1);
}

function eng_switchStateByCode(stateCode) {
	if (stateCode in _eng_gameData['stateCodes']) {
		_eng_switchState(_eng_gameData['stateCodes'][stateCode]);
	} else {
		console.error("Switching to inexistant state code");
	}
}

/** A state has finished without further indications. */
function eng_stateEnded() {
	var state = eng_getCurrentState();
	switch (state["type"]) {
	case "end":
		// The game is over, go back to title screen
		eng_home();
		break;
	case "story":
		// Continue
		if ("next" in state) {
			eng_switchStateByCode(state["next"]);
		} else {
			eng_nextState();
		}
		break;
	}
}

function eng_newGame() {
	_eng_setupInventory(_eng_gameData['startingInventory']);
	_eng_gameState['variables'] = var_regInit();
	_eng_switchState(0);
}

function eng_loadSavedGame(gameName) {
	_eng_gameState = svg_loadGame(eng_getGameCode(), gameName);
	_eng_setupInventory(_eng_gameState["inventory"]);
	_eng_notifyStateChangeObservers();
	_eng_updateHints();
}

function eng_saveGame(saveName) {
	svg_saveGame(eng_getGameCode(), saveName, _eng_gameState);
}

function eng_deleteSaveGame(saveName) {
	svg_deleteGame(eng_getGameCode(), saveName);
}

function eng_hasAutosave() {
	return svg_hasAutosave(eng_getGameCode());
}
function eng_hasSavedGame() {
	return svg_hasSavedGame(eng_getGameCode());
}
function eng_listSavedGames() {
	return svg_listGames(eng_getGameCode());
}

/** Destroy current game state and go to title screen */
function eng_home() {
	_eng_initGameState();
	ttl_show(document.getElementById('display'), _eng_gameData);
}
// Initializing functions
/////////////////////////

var _eng_actionsObservers = [];
/** Add a listener that will be called when action list changes.
 * @param callback (function) Function to be called. Arguments is the
 * new list of actions. */
function eng_observeActionsChange(callback) {
	_eng_actionsObservers.push(callback);
	callback(_eng_gameData['actions']);
}
function eng_unobserveActionsChange(callback) {
	_eng_actionsObservers.remove(callback);
}
function _eng_notifyActionsChangeObservers() {
	for (var i = 0; i < _eng_actionsObservers.length; i++) {
		_eng_actionsObservers[i](_eng_gameData['actions']);
	}
}
var _eng_locationsObservers = [];
/** Add a listener that will be called when locations list changes.
 * @param callback (function) Function to be called. Arguments is the
 * new list of locations. */
function eng_observeLocationsChange(callback) {
	_eng_locationsObservers.push(callback);
	callback(_eng_gameData['locations']);
}
function eng_unobserveLocationsChange(callback) {
	_eng_locationsObservers.remove(callback);
}
function _eng_notifyLocationsChangeObservers() {
	for (var i = 0; i < _eng_locationsObservers.length; i++) {
		_eng_locationsObservers[i](_eng_gameData['locations']);
	}
}
var _eng_translationsObservers = [];
/** Add a listener that will be called when translations are loaded.
 * @param callback (function) Function to be called. Arguments is the
 * translations object. */
function eng_observeTranslations(callback) {
	_eng_translationsObservers.push(callback);
	callback(_eng_gameData['i18nData']);
}
function eng_unobserveTranslations(callback) {
	_eng_translationsObservers.remove(callback);
}
function _eng_notifyTranslationsObservers() {
	for (var i = 0; i < _eng_translationsObservers.length; i++) {
		_eng_translationsObservers[i](_eng_gameData['i18nData']);
	}
}

function eng_loadGame(gameCode, language, onSuccess, onError) {
	if (gameCode == null) {
		// Get game from g param
		gameCode = _get('g');
	}
	if (language == null) {
		// Get language from lang param
		language = _get('lang');
	}
	_eng_gameData['game'] = gameCode;
	if (_eng_gameData['game'] == null) {
		console.error("No game found with given code");
		onError();
		return;
	}
	// Load style
	var gameCssElement = document.createElement('link');
	gameCssElement.setAttribute('rel', 'stylesheet');
	gameCssElement.setAttribute('type', 'text/css');
	gameCssElement.setAttribute('href', './games/' + escUrl(gameCode) + '	/game.css');
	document.head.appendChild(gameCssElement);
	// Load game data
	ajaxJSON('./games/' + escUrl(gameCode) + '/game.json',
		function(data) {
			var copyTags = ['title', 'title_image',
					'title_bgm', 'footer', 'credits', 'characters',
					'global_actions', 'actions', 'states',
					'hintThresholds'];
			for (var i = 0; i < copyTags.length; i++) {
				var tag = copyTags[i];
				if (tag in data) { _eng_gameData[tag] = data[tag]; }
			}
			_eng_gameData['locations'] = [];
			for (var key in data['inventory']) {
				if (key !== ITM_ITEMSKEY) {
					_eng_gameData['locations'].push(key);
				}
			}
			_eng_gameData['startingInventory'] = data['inventory'];
			for (var i = 0; i < data['states'].length; i++) {
				if ('code' in data['states'][i]) {
					_eng_gameData['stateCodes'][data['states'][i]['code']] = i;
				}
			}
			// Check for game data
			if (!'states' in data) {
				console.error("No states defined in game");
				onError();
				return;
			}
			_eng_loadingStatus['gameLoaded'] = true;
			// Load text and translations
			_eng_gameData['i18nData'] = i18n_init();
			var defaultLanguage = "en";
			if ('language' in data) {
				defaultLanguage = data['language'];
			}
			// Default language files
			var defaultPriority = ENG_I18N_DEFAULT_PRIORITY;
			if (language === null) {
				defaultPriority = ENG_I18N_REQ_PRIORITY; // Set default as the requested language
			}
			var loads = [
				{'type': 'json', 'url': "./res/language-" + escUrl(defaultLanguage) + ".json",
				'done': function(data) { i18_loadLanguage(_eng_gameData['i18nData'], defaultLanguage, data, defaultPriority); },
				'fail': function (jqxhr, textStatus, error) { console.warn("Couldn't load core translation file for " + defaultLanguage + ": " + error); }},
				{'type': 'json', 'url': "./games/" + escUrl(gameCode) + "/i18n/language-" + escUrl(defaultLanguage) + ".json",
				'done': function(data) { i18_loadLanguage(_eng_gameData['i18nData'], defaultLanguage, data, defaultPriority); },
				'fail': function (jqxhr, textStatus, error) { console.warn("Couln't load game translation file for " + defaultLanguage + ": " + error); }},
				];
			// Alternate language files
			if (language != null) {
				loads.push({'type': 'json', 'url': "./res/language-" + escUrl(language) + ".json",
					'done': function(data) { i18_loadLanguage(_eng_gameData['i18nData'], language, data, ENG_I18N_REQ_PRIORITY); },
					'fail': function (jqxhr, textStatus, error) { console.warn("Couldn't load core translation file for " + language + ": " + error); },
				});
				loads.push({'type': 'json', 'url': "./games/" + escUrl(gameCode) + "/i18n/language-" + escUrl(language) + ".json",
					'done': function(data) { i18_loadLanguage(_eng_gameData['i18nData'], language, data, ENG_I18N_REQ_PRIORITY); },
					'fail': function (jqxhr, textStatus, error) { console.warn("Couldn't load game translation file for " + language + ": " + error); },
				});
			}
			parallelLoad(loads, function() { onSuccess(_eng_gameData); }, function() {});
		},
		function(request, status, error) {
			console.error(error);
			onError();
		});
}

// Items and objects
////////////////////

var _eng_inventoryObservers = [];
/** Add a listener that will be called when the inventory changes.
 * @param callback (function) Function to be called. Arguments are the
 * location and the new item. */
function eng_observeInventoryChange(callback) {
	_eng_inventoryObservers.push(callback);
	for (var i = 0; i < _eng_gameData['locations'].length; i++) {
		var location = _eng_gameData['locations'][i];
		callback(location, _eng_gameState['inventory'][location]);
	}
}
function eng_unobserveInventoryChange(callback) {
	_eng_inventoryObservers.remove(callback);
}
function _eng_notifyInventoryChangeObservers(location, item) {
	for (var i = 0; i < _eng_inventoryObservers.length; i++) {
		_eng_inventoryObservers[i](location, item);
	}
}
var _eng_objectsObservers = [];
var ENG_OBJECT_ADD = "add";
var ENG_OBJECT_REM = "rem";
var ENG_OBJECT_SET = "set";
var ENG_OBJECT_DISCOVERED = "disc";
/** Add a listener that will be called when available objects changes.
 * @param callback (function) Function to be called. Arguments are the
 * action (ENG_OBJECT_ADD or ENG_OBJECT_REM) and the object code. */
function eng_observeObjectsChange(callback) {
	_eng_objectsObservers.push(callback);
	callback(ENG_OBJECT_SET, _eng_gameState['inventory'][ITM_ITEMSKEY]);
}
function eng_unobserveObjectsChange(callback) {
	_eng_objectsObservers.remove(callback);
}
function _eng_notifyObjectsChangeObservers(operation, object) {
	for (var i = 0; i < _eng_objectsObservers.length; i++) {
		_eng_objectsObservers[i](operation, object);
	}
}

function _eng_setupInventory(inventory) {
	_eng_gameState['inventory'] = itm_invInit(inventory);
	// Notify everything
	for (var key in _eng_gameState['inventory']) {
		if (key === ITM_ITEMSKEY || key === ITM_MAPITEMSKEY) {
			var items = _eng_gameState['inventory'][key];
			for (var i = 0; i < items.length; i++) {
				if (items[i] !== null) {
					if (key === ITM_ITEMSKEY) {
						_eng_notifyObjectsChangeObservers(ENG_OBJECT_ADD,
								items[i]);
					} else {
						_eng_notifyObjectsChangeObservers(ENG_OBJECT_DISCOVERED,
								items[i]);
					}
				}
			}
		} else {
			_eng_notifyInventoryChangeObservers(key,
					itm_getInventoryItem(_eng_gameState['inventory'],
							key));
		}
	}
}

function eng_hasObject(object) {
	return itm_hasObject(_eng_gameState['inventory'], object);
}
function eng_isEquipped(location, item) {
	return itm_isEquipped(_eng_gameState['inventory'], location, item);
}

// Timers
/////////

function eng_declareTimer(timer) {
	for (var i = 0; i < _eng_gameState["timers"].length; i++) {
		var t = _eng_gameState["timers"][i];
		if (t["code"] == timer["code"]) {
			// Redeclared timer, return the previous one
			return t;
		}
	}
	// Register the new timer and return it
	_eng_gameState["timers"].push(timer);
	return timer;
}