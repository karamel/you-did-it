/* You DID it!
 * 
 * This file is part of You DID it!
 * 
 * You DID it! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You DID it! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * You DID it! uses jQuery, licensed by the jQuery Fundation
 * under the MIT License (see https://jquery.org/license/)
 * and Mustache.js, licensed under the MIT License.
 */
 
// This file contains the result parser. It should be embedded in engine
// and thus can access to engine private methods.
// Prefix: rsl

/** The special result code for invalid result. */
var RSL_DONT_DO_THAT = "__dont_do_that__";
var RSL_SRC_ACTION = "action";
var RSL_SRC_MAP = "map";
var RSL_SRC_TIMER = "timer";

/** Parse and proceed a result object. Returns true if it makes
 * the game progress. */
function rsl_proceedResult(result, source) {
	var progressed = false;
	var moved = false;
	if (result == RSL_DONT_DO_THAT) {
		gme_showTextResult("_you_cant_do_that");
		return false;
	}
	if ('text' in result) {
		// TODO: this is an ugly way to differentiate map and action titles
		switch(source) {
		case RSL_SRC_ACTION: gme_showTextResult(result['text']); break;
		case RSL_SRC_TIMER:
		case RSL_SRC_MAP: gme_showResult(result['text']); break;
		}
	}
	if ("sound" in result) {
		snd_playSound(result["sound"]);
	}
	if ('item' in result) {
		if (Array.isArray(result['item'])) {
			for (var i = 0; i < result['item'].length; i++) {
				if (itm_addObject(_eng_gameState['inventory'], result['item'][i])) {
					_eng_notifyObjectsChangeObservers(ENG_OBJECT_ADD, result['item'][i]);
				}
			}
		} else {
			if (itm_addObject(_eng_gameState['inventory'], result['item'])) {
				_eng_notifyObjectsChangeObservers(ENG_OBJECT_ADD, result['item']);
			}
		}
		progressed = true;
	}
	if ('remove_item' in result) {
		if (Array.isArray(result['remove_item'])) {
			for (var i = 0; i < result['remove_item'].length; i++) {
				itm_removeObject(_eng_gameState['inventory'], result['remove_item'][i]);
				_eng_notifyObjectsChangeObservers(ENG_OBJECT_REM, result['remove_item'][i]);
			}
		} else {
			itm_removeObject(_eng_gameState['inventory'], result['remove_item']);
			_eng_notifyObjectsChangeObservers(ENG_OBJECT_REM, result['remove_item']);
		}
		progressed = true;
	}
	if ('inventory' in result) {
		for (var loc in result['inventory']) {
			itm_setItem(_eng_gameState['inventory'], loc, result['inventory'][loc]);
			_eng_notifyInventoryChangeObservers(loc, result['inventory'][loc]);
		}
		progressed = true;
	}
	if ('remove_inventory' in result) {
		for (var loc in result['remove_inventory']) {
			itm_removeItem(_eng_gameState['inventory'], loc, result['remove_inventory'][loc]);
			_eng_notifyInventoryChangeObservers(loc, null);
		}
		progressed = true;
	}
	if ('set' in result) {
		var varDict = result['set'];
		for (var variable in varDict) {
			var_setVar(_eng_gameState['variables'], variable, varDict[variable]);
		}
		progressed = true;
	}
	// Proceed move_to at the end for autosave to keep the right state
	if ('move_to' in result) {
		for (var i = 0; i < _eng_gameState["timers"].length; i++) {
			tmr_stopStateTimer(_eng_gameState["timers"][i]);
		}
		if (result['move_to'] == '_next') {
			eng_nextState();
		} else {
			eng_switchStateByCode(result['move_to']);
		}
		moved = true;
		progressed = true;
	}
	if (!moved) {
		tmr_tick(_eng_gameState["timers"]);
	}
	return progressed;
}