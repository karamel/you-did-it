/* You DID it!
 * 
 * This file is part of You DID it!
 * 
 * You DID it! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You DID it! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * You DID it! uses jQuery, licensed by the jQuery Fundation
 * under the MIT License (see https://jquery.org/license/)
 * and Mustache.js, licensed under the MIT License.
 */
 
// This file contains sound management

// prefix: snd

var SND_BGMID = "_bgm_chanel";
var _snd_currentBGM = null;

function snd_init() {
	var bgmElem = document.createElement('audio');
	bgmElem.setAttribute('id', SND_BGMID);
	bgmElem.setAttribute('loop', '');
	document.body.appendChild(bgmElem);
}

function snd_stopBGM() {
	var bgmElem = document.getElementById(SND_BGMID);
	bgmElem.pause();
	bgmElem.removeAttribute('src');
	_snd_currentBGM = null;
}

/** Set the repeated background music. */
function snd_playBGM(bgm) {
	if (_snd_currentBGM == bgm) {
		return;
	}
	var bgmElem = document.getElementById(SND_BGMID);
	bgmElem.pause();
	bgmElem.setAttribute('src', './games/' + escAttr(escUrl(eng_getGameCode())) + '/' + escAttr(bgm));
	bgmElem.load();
	bgmElem.oncanplaythrough = bgmElem.play;
	_snd_currentBGM = bgm;
}

/** Play a short sound once */
function snd_playSound(sound) {
	var sfxElem = new Audio();
	sfxElem.setAttribute("src", "./games/" + escAttr(escUrl(eng_getGameCode())) + "/" + escAttr(sound));
	sfxElem.load();
	sfxElem.oncanplaythrough = sfxElem.play;
}