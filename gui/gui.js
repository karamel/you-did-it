/* You DID it!
 * 
 * This file is part of You DID it!
 * 
 * You DID it! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You DID it! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * You DID it! uses jQuery, licensed by the jQuery Fundation
 * under the MIT License (see https://jquery.org/license/)
 * and Mustache.js, licensed under the MIT License.
 */
 
// This file contains graphical user interface (GUI) functions
// Prefix: gui


// State GUI
////////////

_gui_currentStateType = null;

function gui_stateChanged(newState) {
	if (newState == null) {
		return;
	}
	if (newState['type'] != _gui_currentStateType) {
		switch (_gui_currentStateType) {
		case 'game':
			gme_close();
			break;
		case 'story':
			str_close();
			break;
		}
	}
	_gui_currentStateType = newState['type'];
	if ("bgm" in newState) {
		snd_playBGM(newState["bgm"]);
	} else {
		snd_stopBGM();
	}
	switch (newState['type']) {
	case 'game':
		gme_show(document.getElementById('display'), newState);
		break;
	case 'story':
	case 'end':
		str_show(document.getElementById('display'), newState);
		break;
	}
	kbd_registerKey(KBD_ESC, gui_openSettings);
}

/** Initialize main UI with loaded languages. */
function gui_setupUI(gameData) {
	// Init translations
	gui_translationsLoaded(gameData['i18nData']);
	// Set listeners
	eng_observeStateChange(gui_stateChanged);
	eng_observeTranslations(gui_translationsLoaded);
	// Init sound
	snd_init();
	// Init keyboard
	kbd_setup();
}

function gui_close() {
	gui_closeSettings();
	switch (_gui_currentStateType) {
	case 'game':
		gme_close();
		break;
	case 'story':
		str_close();
		break;
	}
	_gui_currentStateType = null;
}

// Translations
///////////////
var _gui_i18nData = null;
function gui_translationsLoaded(i18nData) {
	_gui_i18nData = i18nData;
}

function _gui_translate(string, array) {
	return i18_translate(_gui_i18nData, string, array);
}

/** Check if a translation is available in the loaded translation data.
 * @param string The key to look translation for.
 * @param priority (optional) The minimum priority requested.
 */
function _gui_hasTranslation(string, priority) {
	switch (arguments.length) {
	case 1: priority = null;
	}
	return i18_hasTranslation(_gui_i18nData, string, priority);
}

// Quick menu
/////////////

function gui_openSettings() {
	kbd_registerKey(KBD_ESC, gui_closeSettings);
	_gui_showSettings(document.getElementById('display'));
}

function gui_closeSettings() {
	kbd_registerKey(KBD_ESC, gui_openSettings);
	var settingsElem = document.getElementById('settings-screen');
	if (settingsElem != null) {
		settingsElem.parentNode.removeChild(settingsElem);
	}
}

function _gui_showSettings(containerElem) {
	var hasLoad = null;
	if (eng_hasSavedGame()) {
		hasLoad = 'onclick="javascript:gui_settingsLoad();"';
	} else {
		hasLoad = 'class="disabled"';
	}
	var html = Mustache.render(tpl_menuPopup,
			{"menu": _gui_translate("_Menu"),
			"main_menu": _gui_translate("Main menu"),
			"save": _gui_translate("_Save game"),
			"load": _gui_translate("_Load game"),
			"close": _gui_translate("_Close"),
			"hasLoad": hasLoad});
	var settingsElem = document.createElement('div');
	settingsElem.setAttribute('id', 'settings-screen');
	settingsElem.innerHTML = html;
	containerElem.appendChild(settingsElem);
}

function gui_openSaveList(operation) {
	kbd_registerKey(KBD_ESC, gui_closeSaveList);
	_gui_showSaveList(document.getElementById('display'), operation);
}

function _gui_showSaveList(containerElem, operation) {
	var saveTitle = null;
	var title = "";
	var games = eng_listSavedGames();
	var gameList = []
	for (var i = 0; i < games.length; i++) {
		gameList.push({"id": i, "name": games[i]});
	}
	switch (operation) {
	case 'save':
		saveTitle = _gui_translate('_Save game');
		title = _gui_translate('_Save game');
		break;
	case 'load': title = _gui_translate('_Load game'); break;
	case 'delete': title = _gui_translate('_Delete game'); break;
	}
	var html = Mustache.render(tpl_saveList, {
			"title": title,
			"operation": operation,
			"saveTitle": saveTitle,
			"load": _gui_translate("_Load game"),
			"save": _gui_translate("_Save"),
			"close": _gui_translate("_Close"),
			"saveList": gameList,
			});
	var saveListElem = document.createElement('div');
	saveListElem.setAttribute('id', 'savelist-screen');
	saveListElem.innerHTML = html;
	containerElem.appendChild(saveListElem);
}
function gui_closeSaveList() {
	if (document.getElementById('settings-screen') != null) {
		kbd_registerKey(KBD_ESC, gui_closeSettings);
	} else {
		// TODO: ugly hack not to open settings screen on title screen
		kbd_dropKey(KBD_ESC);
	}
	var listElem = document.getElementById('savelist-screen');
	listElem.parentNode.removeChild(listElem);
}
function gui_saveGame() {
	var saveName = document.getElementById('game-name').value.trim();
	if (saveName != "") {
		eng_saveGame(saveName);
		gui_closeSaveList();
		// Refresh menu
		gui_closeSettings();
		gui_openSettings();
		return true;
	}
	return false;
}
function gui_pickSavedGame(gameIndex) {
	var games = eng_listSavedGames();
	var game = games[gameIndex];
	var operation = document.getElementById('savelist-content').classList[0];
	if (operation === 'save') {
		// Update the game name for saving and that's all
		document.getElementById('game-name').setAttribute('value', game);
		return;
	}
	var titleScreenElem = document.getElementById('title-screen');
	switch (operation) {
	case 'load':
		// Close save list and load the game
		gui_closeSaveList();
		gui_closeSettings();
		if (titleScreenElem != null) {
			// Ugly hack to detect if load from title screen
			ttl_close();
		}
		eng_loadSavedGame(game);
		break;
	case 'delete':
		// Delete the game and refresh
		eng_deleteSaveGame(game);
		var hasSavedGame = eng_hasSavedGame();
		if (titleScreenElem != null && !hasSavedGame) {
			// No more saved games, refresh title screen (TODO: ugly)
			ttl_close();
			ttl_show(document.getElementById('display'), _eng_gameData);
		} else {
			// Refresh save list
			gui_closeSaveList();
			gui_openSaveList('delete');
		}
		break;
	}
}

function gui_settingsSave() {
	gui_openSaveList('save');
}
function gui_settingsLoad() {
	gui_openSaveList('load');
}
function gui_settingsDelete() {
	gui_openSaveList('delete');
}
function gui_settingsHome() {
	gui_close();
	eng_home();
}