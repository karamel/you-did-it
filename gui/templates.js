/* You DID it!
 * 
 * This file is part of You DID it!
 * 
 * You DID it! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You DID it! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * You DID it! uses jQuery, licensed by the jQuery Fundation
 * under the MIT License (see https://jquery.org/license/)
 * and Mustache.js, licensed under the MIT License.
 */
 
// This file contains the templates for Mustache.js
// Prefix: tpl

var tpl_titleScreen = '<div id="title-screen" {{#bgImage}}style="background-image:url(./games/{{bgImage}}"{{/bgImage}}>'
	+'{{#title}}<h1 id="title">{{title}}</h1>{{/title}}'
	+'<ul id="title-choices">'
		+'<li onclick="javascript:ttl_newGame()">{{new_game}}</li>'
		+'<li {{{hasContinue}}}>{{continue}}</li>'
		+'<li {{{hasSaved}}}>{{load}}</li>'
		+'<li {{{hasDelete}}}>{{delete}}</li>'
		+'{{#hasCredits}}<li onclick="javascript:ttl_showCredits()">{{creditsTitle}}</li>{{/hasCredits}}'
	+'</ul>'
	+'{{#hasCredits}}'
	+'<ul id="credits" class="hidden" onclick="javascript:ttl_closeCredits()">'
		+'{{#credits}}<li>'
			+'<h3>{{section}}</h3>'
			+'<ul class="credit-section-content">'
			+'{{#content}}<li>{{name}}</li>{{/content}}'
			+'</ul>'
		+'</li>{{/credits}}'
	+'</ul>{{/hasCredits}}'
	+'{{#footer}}<div id="footer">{{{footer}}}</div>{{/footer}}'
	+'<div id="disclaimer"><p>Powered by <a href="http://youdidit.creativekara.fr">You DID it!</a></p></div>'
+'</div>';


var tpl_storyScreen = '<div id="story" {{#class}}class="story-{{{class}}}"{{/class}}>'
	+'{{#picture}}<div id="story-image">'
		+'{{#bigPicture}}<a href="./games/{{bigPicture}}">{{/bigPicture}}'
		+'<img src="./games/{{picture}}" />'
		+'{{#bigPicture}}</a>{{/bigPicture}}'
	+'</div>{{/picture}}'
	+'<div id="story-text"></div>'
	+'<button id="story-continue" onclick="javascript:str_continue()">{{continue}}</button>'
+'</div>';


var tpl_gameScreen = '<div id="main">'
	+'<div id="picture">'
		+'<a href="#" target="_new"><img/></a>'
	+'</div>'
	+'<div id="map">'
		+'<img usemap="#map"/>'
		+'<map name="map"/>'
	+'</div>'
	+'<ul id="hints"></ul>'
	+'<div id="result" class="hidden" onclick="javascript:gme_closeResult();"></div>'
	+'<div id="action-bar" style="background-color:#555">'
		+'<div id="items">'
			+'<div id="inventory">'
				+'<div class="title">{{_inventory}}</div>'
				+'<ul></ul>'
			+'</div>'
			+'<div id="objects">'
				+'<div class="title">{{_objects}}</div>'
				+'<ul></ul>'
			+'</div>'
		+'</div>'
		+'<div id="notification">'
			+'<div class="title"></div>'
		+'</div>'
		+'<div id="actions">'
			+'<ul></ul>'
		+'</div>'
	+'</div>'
+'</div>';

var tpl_gameResult = '{{#title}}<div class="title">{{title}}</div>{{/title}}'
+'<div id="result-text"></div>'
+'<div id="click-to-close">{{close}}</div>';

var tpl_gameHint = '<li onclick="javascript:gme_hintClicked({{id}});">{{title}}</li>';

var tpl_menuPopup = '<div id="settings-content">'
	+'<h2 id="settings-title">{{menu}}</h2>'
	+'<ul id="settings-choices">'
		+'<li onclick="javascript:gui_settingsHome()">{{main_menu}}</li>'
		+'<li onclick="javascript:gui_settingsSave();">{{save}}</li>'
		+'<li {{{hasLoad}}}>{{load}}</li>'
		+'<li onclick="javascript:gui_closeSettings();">{{close}}</li>'
	+'</ul>'
+'</div>';

var tpl_saveList = '<div id="savelist-content" class="{{operation}}">'
	+'<h2 id="savelist-title">{{title}}</h2>'
	+'<ul id="savelist-choices">'
		+'{{#saveTitle}}<li><input id="game-name" type="text" /><button onclick="javascript:gui_saveGame();">{{save}}</button></li>{{/saveTitle}}'
		+'{{#saveList}}<li onclick="javascript:gui_pickSavedGame({{id}})">{{name}}</li>{{/saveList}}'
		+'<li onclick="javascript:gui_closeSaveList();">{{close}}</li>'
	+'</ul>'
+'</div>';
