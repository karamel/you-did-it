/* You DID it!
 * 
 * This file is part of You DID it!
 * 
 * You DID it! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You DID it! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * You DID it! uses jQuery, licensed by the jQuery Fundation
 * under the MIT License (see https://jquery.org/license/)
 * and Mustache.js, licensed under the MIT License.
 */
 
// This file contains the starting script.

function gameNotLoaded() {
	document.getElementById('display').innerHTML = 'Game not loaded';
}

function gameLoaded(gameData) {
	gui_setupUI(gameData);
	eng_home();
}

function start(game, language) {
	switch (arguments.length) {
	case 0: game = null;
	case 1:	language = null;
	}
	eng_loadGame(game, language, gameLoaded, gameNotLoaded);
}