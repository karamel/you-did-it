/* You DID it!
 * 
 * This file is part of You DID it!
 * 
 * You DID it! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You DID it! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * You DID it! uses jQuery, licensed by the jQuery Fundation
 * under the MIT License (see https://jquery.org/license/)
 * and Mustache.js, licensed under the MIT License.
 */
 
// This file contains title screen displayer

// prefix: ttl

function ttl_show(containerElem, gameData) {
	var bgImage = null;
	var title = null;
	var hasContinue = null;
	var hasSaved = null;
	var hasDelete = null;
	var footer = null;
	var credits = null;
	if ("title_image" in gameData) {
		bgImage = escUrl(gameData["game"] + "/" + gameData["title_image"]);
	}
	if ("title" in gameData) {
		title = _gui_translate(gameData["title"]);
	}
	if ("footer" in gameData) {
		footer = _gui_translate(gameData["footer"]);
	}
	if ("credits" in gameData) {
		credits = gameData["credits"];
	}
	if (eng_hasAutosave()) {
		hasContinue = 'onclick="javascript:ttl_continue();"';
	} else {
		hasContinue = 'class="disabled"';
	}
	if (eng_hasSavedGame()) {
		hasSaved = 'onclick="javascript:gui_openSaveList(\'load\');"';
		hasDelete = 'onclick="javascript:gui_openSaveList(\'delete\');"';
	} else {
		hasSaved = 'class="disabled"';
		hasDelete = 'class="disabled"';
	}
	var html = Mustache.render(tpl_titleScreen, {"bgImage": bgImage,
			"title": title,
			"new_game": _gui_translate("_New game"),
			"continue": _gui_translate("_Continue"),
			"load": _gui_translate("_Load game"),
			"delete": _gui_translate("_Delete game"),
			"footer": footer,
			"creditsTitle": _gui_translate("_Credits"),
			"hasCredits": (credits != null),
			"credits": credits,
			"hasContinue": hasContinue, "hasSaved": hasSaved,
			"hasDelete": hasDelete});
	// BGM
	if ("title_bgm" in gameData) {
		snd_playBGM(gameData["title_bgm"]);
	} else {
		snd_stopBGM();
	}
	// Disable small menu
	kbd_dropKey(KBD_ESC);
	containerElem.innerHTML = html;
}

function ttl_close() {
	snd_stopBGM();
}

function ttl_newGame() {
	ttl_close();
	eng_newGame();
}

function ttl_continue() {
	ttl_close();
	eng_loadSavedGame(SVG_AUTOSAVE_NAME);
}

function ttl_showCredits() {
	showElem(document.getElementById('credits'));
}
function ttl_closeCredits() {
	hideElem(document.getElementById('credits'));
}