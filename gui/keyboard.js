/* You DID it!
 * 
 * This file is part of You DID it!
 * 
 * You DID it! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You DID it! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * You DID it! uses jQuery, licensed by the jQuery Fundation
 * under the MIT License (see https://jquery.org/license/)
 * and Mustache.js, licensed under the MIT License.
 */
 
// This file contains keyboard management

// prefix: kbd

var KBD_ENTER = "Enter";
var KBD_SPACE = " ";
var KBD_ESC = "Escape";

var _kbd_bindings = {};

/** Initialize the keyboard handler. */
function kbd_setup() {
	document.addEventListener('keydown', kbd_handleKey);
}

function kbd_registerKey(key, callback) {
	_kbd_bindings[key] = callback;
}

function kbd_dropKey(key) {
	delete _kbd_bindings[key];
}

function kbd_handleKey(event) {
	var char = event["key"];
	if (char in _kbd_bindings) {
		_kbd_bindings[char]();
	}
}