/* You DID it!
 * 
 * This file is part of You DID it!
 * 
 * You DID it! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You DID it! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * You DID it! uses jQuery, licensed by the jQuery Fundation
 * under the MIT License (see https://jquery.org/license/)
 * and Mustache.js, licensed under the MIT License.
 */
 
// This file contains items and inventory management functions.
// Prefix: itm

var ITM_ITEMSKEY = "_items";
var ITM_MAPITEMSKEY = "_mapItems";

/** Initialize an inventory from data.
 * @param data (optional associative array) dictionary of locations
 * and equiped item. The special optional _items key is an array with
 * starting inventory. If omited it will be initialized empty. */
function itm_invInit(data) {
	switch (arguments.length) {
	case 0:
		// No data given
		return {"_items": [], "_mapItems": []};
	}
	var inv = {};
	var items = [];
	var mapItems = [];
	for (var key in data) {
		if (key === ITM_ITEMSKEY) {
			if (Array.isArray(data[key])) {
				items = data[key];
			} else {
				items.push(data[key]);
			}
		} else if (key === ITM_MAPITEMSKEY) {
			if (Array.isArray(data[key])) {
				mapItems = data[key];
			} else {
				mapItems.push(data[key]);
			}
		} else {
			inv[key] = data[key];
		}
	}
	inv[ITM_ITEMSKEY] = items;
	inv[ITM_MAPITEMSKEY] = mapItems;
	return inv;
}

function itm_getInventoryItem(inv, location) {
	if (location in inv) {
		return inv[location];
	} else {
		console.warn("Trying to get an item from undefined location.");
		return null;
	}
}

/** Set an item in given inventory location and update display.
 * @param inv The inventory object.
 * @param location The inventory location code.
 * @param item The item code to set. */
function itm_setItem(inv, location, item) {
	// Add to inventory
	inv[location] = item;
}

/** Remove an item from inventory.
 * @param inv The inventory object.
 * @param location The location code to remove the item from.
 * @param item The item code to remove. */
function itm_removeItem(inv, location, item) {
	if (location in inv && inv[location] == item) {
		itm_setItem(inv, location, null);
	}
}

/** Check if an item is equipped. */
function itm_isEquipped(inv, location, item) {
	if (location in inv && inv[location] == item) {
		return true;
	}
	return false;
}

/** Add an item to available ones.
 * @param inv The inventory object.
 * @return True if object is added, false if already there. */
function itm_addObject(inv, item) {
	// Add object if not already in list
	for (var i = 0; i < inv[ITM_ITEMSKEY].length; i++) {
		if (inv[ITM_ITEMSKEY][i] == item) {
			return false;
		}
	}
	inv[ITM_ITEMSKEY].push(item);
	return true;
}

/** Remove an item from available ones. */
function itm_removeObject(inv, item) {
	for (var i = 0; i < inv[ITM_ITEMSKEY].length; i++) {
		if (inv[ITM_ITEMSKEY][i] == item) {
			inv[ITM_ITEMSKEY].splice(i, 1);
			return;
		}
	}
}

/** Check if an item is in inventory. */
function itm_hasObject(inv, item) {
	for (var key in inv) {
		if (key ===ITM_ITEMSKEY) {
			for (var i = 0; i < inv[ITM_ITEMSKEY].length; i++) {
				if (inv[ITM_ITEMSKEY][i] === item) {
					return true;
				}
			}
		} else {
			if (inv[key] === item) {
				return true;
			}
		}
	}
	return false;
}

function itm_isDiscovered(inv, item) {
	for (var i = 0; i < inv[ITM_MAPITEMSKEY].length; i++) {
		if (inv[ITM_MAPITEMSKEY][i] ==  item) {
			return true;
		}
	}
	return false;
}

function itm_discoverItem(inv, item) {
	if (!itm_isDiscovered(inv, item)) {
		inv[ITM_MAPITEMSKEY].push(item);
		return true;
	}
	console.warn("Trying to discover " + item + " which is already discovered");
	return false;
}