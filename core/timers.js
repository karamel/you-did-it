/* You DID it!
 * 
 * This file is part of You DID it!
 * 
 * You DID it! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You DID it! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * You DID it! uses jQuery, licensed by the jQuery Fundation
 * under the MIT License (see https://jquery.org/license/)
 * and Mustache.js, licensed under the MIT License.
 */

// This file contains timer functions.
// Prefix: tmr

function tmr_initTimer(code, time, result) {
	return {"code": code,
		"remaining": time,
		"result": result,
		"active": false
	};
}

/** Start a state timer */
function tmr_startStateTimer(timer) {
	timer["active"] = true;
}

function tmr_stopStateTimer(timer) {
	timer["active"] = false;
}

/** Tick all active timers in the given set.
 * May trigger result if a timer is out */
function tmr_tick(timers) {
	for (var i = 0; i < timers.length; i++) {
		var timer = timers[i];
		if (timer["active"]) {
			timer['remaining']--;
			if (timer['remaining'] <= 0) {
				tmr_stopStateTimer(timer);
				rsl_proceedResult(timer['result'], RSL_SRC_TIMER);
			}
		}
	}
}