module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        preserveComments: false,
      },
      my_targets: {
        files: {
          'dist/<%= pkg.name %>-<%= pkg.version %>.min.js': ['libs/*.js', 'core/*.js', 'gui/*.js', 'start.js']
        }
      }
    },
    cssmin: {
      target: {
        files : [{
          src: ['res/structure.css'],
          dest: 'dist/<%= pkg.name %>-<%= pkg.version %>.min.css'
        }]
      }
    },
    concat: {
      options: {
        process: function(src, filepath) {
          switch (filepath.substring(filepath.length - 3)) {
          case '.js': return '<script type="text/javascript">' + src.replace('</script>', '<\\/script>') + '</script>';
          case 'css': return '<style type="text/css">' + src + '</style>';
          default: return src;
          }
        }
      },
      dist: {
        src: ['index.header.txt',
              'dist/<%= pkg.name %>-<%= pkg.version %>.min.js',
              'dist/<%= pkg.name %>-<%= pkg.version %>.min.css',
              'index.content.txt'],
        dest: 'dist/index.html'
      }
    },
    clean: ['dist/*']
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-clean');
  
  grunt.registerTask('dist', ['clean', 'uglify', 'cssmin', 'concat']);
};
