/* You DID it!
 * 
 * This file is part of You DID it!
 * 
 * You DID it! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You DID it! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * You DID it! uses jQuery, licensed by the jQuery Fundation
 * under the MIT License (see https://jquery.org/license/)
 * and Mustache.js, licensed under the MIT License.
 */
 
// This file contains the "if" content parser
// Prefix: cnd

// Step1: parser for and, or and boolean data
// Step2: parser for is in, is not in
// Step3: recursive parser for parenthesis

var CND_OR = "or";
var CND_AND = "and";
var CND_IN = "is in";
var CND_IN2 = "in";
var CND_NOT_IN = "is not in";
var CND_NOT_IN2 = "not in";
var CND_ITEMS = "items";
var CND_IS = "is";
var CND_IS_NOT = "is not";
var CND_TIMES = "times out of";
var CND_TIMES2 = "of";

/** Process if data.
 * @param data Array of condition elements (if content).
 * @param gameData Game data to make checks in.
 * @return True if conditions are met, false otherwise,
 * null in case of syntax error. */
function cnd_processIf(data, gameData) {
	var stack = []
	for (var i = 0; i < data.length; i++) {
		// Add a new element on stack and (re)parse
		if (Array.isArray(data[i])) {
			// Process the subdata and add them
			var result = cnd_processIf(data[i], gameData);
			if (result != null) {
				stack.push(result);
			} else {
				// Syntax error
				return null;
			}
		} else {
			// Add an element to stack to process
			stack.push(data[i]);
		}
		while (_cnd_proceedStack(stack, gameData) != false) {
			// Loop on _cnd_proceedStack until it stops
		}
	}
	// Everything is parsed, check if the result is there
	if (stack.length == 1) {
		if (stack[0] == true || stack[0] == false) {
			return stack[0];
		}
	}
	// Some elements can't be parsed, it's a syntax error
	return null;
}

/** Check if an element is a keyword or a variable. */
function cnd_isKeyword(element) {
	return element == CND_OR || element == CND_AND || element == CND_IN
			|| element == CND_IN2 || element == CND_NOT_IN
			|| element == CND_NOT_IN2 || element == CND_ITEMS
			|| element == CND_IS || element == CND_IS_NOT;
}

/** Try to read the stack and convert a known expression to a boolean.
 * @param stack The array of elements to proceed.
 * @param gameData The game data to check in.
 * @return True if an expression was found and converted in stack,
 * false if nothing changed. */
function _cnd_proceedStack(stack, gameData) {
	// Read by chunk of three from the end and try to merge
	// This works only if all expressions requires 3 elements
	if (stack.length < 3) {
		return false;
	}
	// Extract last operands and operator and execute it
	var i = stack.length - 3;
	var result = _cnd_parse(stack.slice(i, i + 3), gameData);
	if (result != null) {
		// Replace the chunk by the result
		stack.splice(i, 3, result);
		return true;
	}
	return false;
}

/** Try to parse a chunk of data.
 * @param data An array of condition elements.
 * @param gameData Game data to check in.
 * @return True or false if parsed, null if invalid. */
function _cnd_parse(data, gameData) {
	var operand = data[1];
	if (operand == CND_OR) {
		return _cnd_parseOr(data[0], data[2]);
	} else if (operand == CND_AND) {
		return _cnd_parseAnd(data[0], data[2]);
	} else if (operand == CND_IS) {
		return _cnd_parseIs(data[0], data[2], gameData);
	} else if (operand == CND_IS_NOT) {
		return _cnd_parseIsNot(data[0], data[2], gameData);
	} else if (operand == CND_IN || operand == CND_IN2) {
		return _cnd_parseIsIn(data[0], data[2], gameData);
	} else if (operand == CND_NOT_IN || operand == CND_NOT_IN2) {
		return _cnd_parseIsNotIn(data[0], data[2], gameData);
	} else if (operand == CND_TIMES || operand == CND_TIMES2) {
		return _cnd_parseTimes(data[0], data[2]);
	}
	return null;
}

function _cnd_parseOr(elem1, elem2) {
	if ((elem1 == true || elem1 == false)
			&& (elem2 == true || elem2 == false)) {
		return elem1 || elem2;
	}
	return null;
}

function _cnd_parseAnd(elem1, elem2) {
	if ((elem1 == true || elem1 == false)
			&& (elem2 == true || elem2 == false)) {
		return elem1 && elem2;
	}
	return null;
}

function _cnd_parseIs(elem1, elem2, gameData) {
	return var_getVar(gameData['variables'], elem1) == elem2;
}

function _cnd_parseIsNot(elem1, elem2, gameData) {
	return var_getVar(gameData['variables'], elem1) != elem2;
}

function _cnd_parseIsIn(elem1, elem2, gameData) {
	if (elem2 == CND_ITEMS) {
		return itm_hasObject(gameData['inventory'], elem1);
	} else {
		return itm_isEquipped(gameData['inventory'], elem2, elem1);
	}
}

function _cnd_parseIsNotIn(elem1, elem2, gameData) {
	if (elem2 == CND_ITEMS) {
		return !itm_hasObject(gameData['inventory'], elem1);
	} else {
		return !itm_isEquipped(gameData['inventory'], elem2, elem1);
	}
}

function _cnd_parseTimes(elem1, elem2) {
	if (typeof elem1 == "string") {
		elem1 = parseFloat(elem1);
	}
	if (typeof elem2 == "string") {
		elem2 = parseFloat(elem2);
	}
	return (Math.random() * elem2 < elem1);
}