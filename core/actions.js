/* You DID it!
 * 
 * This file is part of You DID it!
 * 
 * You DID it! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You DID it! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * You DID it! uses jQuery, licensed by the jQuery Fundation
 * under the MIT License (see https://jquery.org/license/)
 * and Mustache.js, licensed under the MIT License.
 */
 
// This file contains action picking and parsing automat.
// Prefix: act

/** State: wating for picking an action. */
var ACT_PICK_ACTION = 0;
/** State: action picked, waiting for picking targets. */
var ACT_PICK_TARGET = 1;
var ACT_WILDCARD = "*";

/** (private) In memory action data for picking.
 * state: (enum) current picking state, see constants.
 * action_picked: (action object) the action picked.
 * targets_picked: (array of string) the array of picked target codes. */
var _act_state = {
	"state": ACT_PICK_ACTION,
	"action_picked": null,
	"targets_picked": [],
}

// public _act_state getters and setters
function act_getActionState() {
	return _act_state['state'];
}
function act_getActionPicked() {
	return _act_state['action_picked'];
}
function act_getTargetPicked(i) {
	return _act_state['targets_picked'][i];
}
function act_getTargetsPickedCount() {
	return _act_state['targets_picked'].length;
}
function act_resetState() {
	_act_setActionState(ACT_PICK_ACTION);
}

// private _act_state setters
/** Set action state and do on-state-entering stuff. */
function _act_setActionState(state) {
	// State leaving code
	// ...nothing for now...
	// State entering code
	switch (state) {
	case ACT_PICK_ACTION:
		// Reset automat
		_act_state['action_picked'] = null;
		_act_state['targets_picked'] = [];
		break;
	case ACT_PICK_TARGET:
		break;
		break;
	}
	_act_state['state'] = state;
}
function _act_setActionPicked(action) {
	_act_state['action_picked'] = action;
}
function _act_resetTargets() {
	_act_state['targets_picked'] = [];
}
function _act_addTarget(target) {
	_act_state['targets_picked'].push(target);
}

/** Built the default "do nothing" result with current action. */
function _act_buildDefaultResult() {
	var defaultResult = [];
	defaultResult['action'] = [act_getActionPicked()['code']];
	for (var i = 0; i < act_getTargetsPickedCount(); i++) {
		defaultResult['action'].push(act_getTargetPicked(i));
	}
	defaultResult['result'] = RSL_DONT_DO_THAT;
	return defaultResult;
}

/** Check if an action triggers the default result "you can't do that". */
function act_isDefaultResult(action) {
	return action != null && (action["result"] == RSL_DONT_DO_THAT);
}

/** Clicked on an action, if action is not already picked pick it
 * and expect a target. Otherwise reset action picking with new one.
 * @return true if something has changed, false otherwise. */
function act_actionPicked(action) {
	if (_act_state['action_picked'] == action) {
		return false;
	}
	_act_setActionPicked(action);
	_act_resetTargets();
	_act_setActionState(ACT_PICK_TARGET);
	return true;
}

/** Clicked on a target. */
function act_targetPicked(target_code) {
	if (act_getActionState() == ACT_PICK_TARGET) {
		// Add the selected target to action
		_act_addTarget(target_code);
	}
}

/** Check if the current automat can trigger an action in the given one.
 * @return The first action object that can be triggered, null if none. */
function act_checkActionTriggering(actions) {
	var action = act_getActionPicked();
	if (action == null) {
		console.error("Checking triggering of a null action");
		return null;
	}
	var actCodes = [action['code']];
	for (var i = 0; i < act_getTargetsPickedCount(); i++) {
		actCodes.push(act_getTargetPicked(i));
	}
	for (var i = 0; i < actions.length; i++) {
		// Check if action is the same length as current action,
		// either a full action or a shortcut.
		var actionIter = actions[i]['action'];
		if (actCodes.length != actionIter.length) {
			continue;
		}
		// Check if the action is the same or wildcard and assign it
		var ok = true;
		for (var j = 0; j < actionIter.length; j++) {
			if (actCodes[j] != actionIter[j]
					&& actionIter[j] != ACT_WILDCARD) {
				ok = false;
				break;
			}
		}
		if (ok) {
			return actions[i];
		}
	}
	// No matching action found, check if the automat is full
	if (action['targets'] == act_getTargetsPickedCount()) {
		return _act_buildDefaultResult();
	}
	// Waiting for more...
	return null;
}
