/* You DID it!
 * 
 * This file is part of You DID it!
 * 
 * You DID it! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You DID it! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * You DID it! uses jQuery, licensed by the jQuery Fundation
 * under the MIT License (see https://jquery.org/license/)
 * and Mustache.js, licensed under the MIT License.
 */
 
// This file contains story screen displayer

// prefix: str

var _str_storyHidden = false;
/** Array of writers for the story. */
var _str_story = null;
/** Current writer index. -1 when not started. */
var _str_storyIndex = -1;

function str_show(containerElem, gameState) {
	var picture = null;
	var bigPicture = null;
	var code = null;
	if ("picture" in gameState) {
		picture = escAttr(escUrl(eng_getGameCode()) + "/" + escUrl(gameState['picture']));
	}
	if ("big_picture" in gameState) {
		big_picture = escAttr(escUrl(eng_getGameCode()) + "/" + escUrl(gameState['big_picture']));
	}
	if ('code' in gameState) {
		code = gameState['code'].replace(' ', '_');
	}
	var html = Mustache.render(tpl_storyScreen,
			{"continue": _gui_translate("_Continue"),
			"picture": picture, "bigPicture": bigPicture,
			"class": code});
	containerElem.innerHTML = html;
	_str_setStory(gameState);
	kbd_registerKey(KBD_ENTER, str_continue);
	kbd_registerKey(KBD_SPACE, str_continue);
	kbd_registerKey("h", _str_keyHide);
}

function str_close() {
	_str_stopWriters();
	_str_opened = false;
	kbd_dropKey(KBD_ENTER);
	kbd_dropKey(KBD_SPACE);
	kbd_dropKey("h");
}

/** Handle "hide text"  key press, toggle story on and off. */
function _str_keyHide() {
	_str_storyHidden = !_str_storyHidden;
	_str_updateStoryDisplay();
}

function _str_updateStoryDisplay() {
	if (_str_storyHidden) {
		hideElem(document.getElementById('story-text'));
	} else {
		showElem(document.getElementById('story-text'));
	}
}

/** Stop all writers and clean data. */
function _str_stopWriters() {
	if (_str_story == null) {
		return;
	}
	while (_str_storyIndex < _str_story.length) {
		wrt_stop(_str_story[_str_storyIndex]);
		_str_storyIndex++;
	}
	_str_story = null;
	_str_storyIndex = -1;
}

function str_continue() {
	// If writer is running, display everything
	if (_str_storyIndex != -1 && wrt_isRunning(_str_story[_str_storyIndex])) {
		wrt_stop(_str_story[_str_storyIndex]);
		return;
	}
	// Otherwise advance in story
	_str_storyIndex++;
	if (_str_storyIndex >= _str_story.length) {
		// End of story
		eng_stateEnded();
	} else {
		// Redisplay story if it was hidden
		_str_storyHidden = false;
		_str_updateStoryDisplay();
		// Show next story block
		wrt_start(_str_story[_str_storyIndex]);
	}
}

function _str_setStory(state) {
	_str_stopWriters();
	if ("story" in state) {
		_str_story = [];
		var lines = state["story"];
		if (!Array.isArray(lines)) {
			lines = Array(lines);
		}
		if (String.isString(lines[0])) {
			// Single box story with text only
			var text = [];
			for (var i = 0; i < lines.length; i++) {
				text.push(escHtml(_gui_translate(lines[i])));
			}
			_str_story = Array(wrt_init(document.getElementById('story-text'), text));
		} else {
			// Multi-box VN-style story
			for (var i = 0; i < lines.length; i++) {
				var textbox = lines[i];
				var text = [];
				var options = {};
				if (Array.isArray(lines[i]["text"])) {
					for (var j = 0; j < lines[i]["text"].length; j++) {
						text.push(escHtml(_gui_translate(lines[i]["text"][j])));
					}
				} else {
					text.push(escHtml(_gui_translate(lines[i]["text"])));
				}
				if ("color" in textbox) {
					options["color"] = textbox["color"];
				}
				if ("name" in textbox) {
					options["name"] = _gui_translate(textbox["name"]);
				}
				if ("char" in textbox) {
					var char = eng_getCharacter(textbox["char"]);
					if (char != null) {
						if ("color" in char) {
							options["color"] = char["color"];
						}
						if ("name" in char) {
							options["name"] = _gui_translate(char["name"]);
						}
					}
				}
				_str_story.push(wrt_init(document.getElementById('story-text'), text, options));
			}
		}
		_str_storyIndex = -1;
		str_continue();
	}
}