/* You DID it!
 * 
 * This file is part of You DID it!
 * 
 * You DID it! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You DID it! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * You DID it! uses jQuery, licensed by the jQuery Fundation
 * under the MIT License (see https://jquery.org/license/)
 * and Mustache.js, licensed under the MIT License.
 */
 
// This file contains saving and loading game states.
// Prefix: svg

var SVG_AUTOSAVE_NAME = "__autosave__";

function svg_saveGame(gameCode, gameName, gameState) {
	localStorage.setItem(gameCode + "-" + gameName, JSON.stringify(gameState));
}

function svg_loadGame(gameCode, gameName) {
	return JSON.parse(localStorage.getItem(gameCode + "-" + gameName));
}

function svg_deleteGame(gameCode, gameName) {
	localStorage.removeItem(gameCode + "-" + gameName);
}

function svg_listGames(gameCode) {
	var games = [];
	for (var i = 0; i < localStorage.length; i++) {
		var key = localStorage.key(i);
		if (key.substr(0, gameCode.length + 1) == gameCode + "-") {
			var gameName = key.substr(gameCode.length + 1);
			if (gameName !== SVG_AUTOSAVE_NAME) {
				games.push(gameName);
			}
		}
	}
	return games;
}

function svg_hasAutosave(gameCode) {
	for (var i = 0; i < localStorage.length; i++) {
		var key = localStorage.key(i);
		if (key == gameCode + "-" + SVG_AUTOSAVE_NAME) {
			return true;
		}
	}
	return false;
}

/** Check if some savegames are available, excepting autosave. */
function svg_hasSavedGame(gameCode) {
	for (var i = 0; i < localStorage.length; i++) {
		var key = localStorage.key(i);
		if (key.substr(0, gameCode.length + 1) == gameCode + "-") {
			var gameName = key.substr(gameCode.length + 1);
			if (gameName !== SVG_AUTOSAVE_NAME) {
				return true;
			}
		}
	}
	return false;
}