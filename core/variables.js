/* You DID it!
 * 
 * This file is part of You DID it!
 * 
 * You DID it! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You DID it! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * You DID it! uses jQuery, licensed by the jQuery Fundation
 * under the MIT License (see https://jquery.org/license/)
 * and Mustache.js, licensed under the MIT License.
 */
 
// This file contains in-qame variables management functions.
// Prefix: var

/** Initialize an empty variable registry. */
function var_regInit() {
	return {};
}

/** Assign a value to a custom variable.
 * @param reg The variables registry.
 * @param variable (string) The variable name.
 * @param value (mixed) The new variable value. */
function var_setVar(reg, variable, value) {
	reg[variable]= value;
}

/** Get a custom variable value.
 * @param reg The variable registry,
 * @param variable Variable name.
 * @return Variable value or null if not defined. */
function var_getVar(reg, variable) {
	if (variable in reg) {
		return reg[variable];
	} else {
		return null;
	}
}