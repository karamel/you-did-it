/* You DID it!
 * 
 * This file is part of You DID it!
 * 
 * You DID it! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You DID it! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * You DID it! uses jQuery, licensed by the jQuery Fundation
 * under the MIT License (see https://jquery.org/license/)
 * and Mustache.js, licensed under the MIT License.
 */
 
// This file contains generic tools not directly bound to You DID it!

String.prototype.format = function (args) {
	var str = this;
	return str.replace(String.prototype.format.regex, function(item) {
		var intVal = parseInt(item.substring(1, item.length - 1));
		var replace;
		if (intVal >= 0) {
			replace = args[intVal];
		} else if (intVal === -1) {
			replace = "{";
		} else if (intVal === -2) {
			replace = "}";
		} else {
			replace = "";
		}
		return replace;
	});
};
String.prototype.format.regex = new RegExp("{-?[0-9]+}", "g");

String.isString = function (variable) {
	return (typeof(variable) == "string");
}

function _get( name )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return null;
  else
    return results[1];
}

Array.prototype.remove = function(item) {
	var index = this.indexOf(item);
	if (index == -1) {
		return false;
	} else {
		this.splice(index, 1);
		return true;
	}
}

function isDefined(varName) {
	return typeof(window[varName]) != "undefined";
}

function escHtml(string) {
	return string;
}
function escAttr(string) {
	return string;
}
function escUrl(string) {
	return string;
}
/** Escape string to include it in javascript code, surrounded by ' '
 * and by " " for html element. */
function escJs(string) {
	var esc = string.replace(new RegExp("'", 'g'), "\\'");
	esc = esc.replace(new RegExp("\"", 'g'), "&quot;");
	return esc;
}

function ajaxJSON(url, onSuccess, onError, onEnd) {
	switch (arguments.length) {
	case 3:
		onEnd = null;
	}
	var request = new XMLHttpRequest();
	request.onreadystatechange = function() {
		if (request.readyState === XMLHttpRequest.DONE) {
			if (request.status == 200) {
				try {
					var data = JSON.parse(request.responseText)
					onSuccess(data);
					if (onEnd != null) { onEnd(); }
				} catch (e) {
					onError(request, request.status, e);
					if (onEnd != null) { onEnd(); }
				}
			} else {
				onError(request, request.status, request.responseText);
				if (onEnd != null) { onEnd(); }
			}
		}
	};
	request.open('GET', url);
	try {
		request.send();
	} catch (error) {
		onError(request, request.status, error);
		if (onEnd != null) { onEnd(); }
	}
}

function parallelLoad(targets, done, progress) {
	var loaded = 0;
	function partialDone() {
		loaded++;
		if (loaded == targets.length) {
			done();
		} else {
			progress(loaded);
		}
	}
	for (var i = 0; i < targets.length; i++) {
		var target = targets[i];
		if ('type' in target && target['type'] == 'json') {
			ajaxJSON(target['url'], target['done'], target['fail'], partialDone);
		}
	}
}

function hideElem(elem) {
	elem.classList.add('hidden');
}
function showElem(elem) {
	elem.classList.remove('hidden');
}