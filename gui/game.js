/* You DID it!
 * 
 * This file is part of You DID it!
 * 
 * You DID it! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You DID it! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * You DID it! uses jQuery, licensed by the jQuery Fundation
 * under the MIT License (see https://jquery.org/license/)
 * and Mustache.js, licensed under the MIT License.
 */
 
// This file contains game screen displayer

// prefix: gme

var _gme_storyWriter = null;
var _gme_resultWriter = null;
var _gme_hintsShown = 0;
var _gme_hints = [];
var _gme_opened = false;

function gme_getHintsShownCount() {
	return _gme_hintsShown;
}

function gme_show(containerElem, gameState) {
	if (!_gme_opened) {
		containerElem.innerHTML = Mustache.render(tpl_gameScreen,
				{"_inventory": _gui_translate("_inventory"),
				"_objects": _gui_translate("_objects")});
		eng_observeLocationsChange(gme_locationsChanged);
		eng_observeActionsChange(gme_actionsChanged);
		eng_observeInventoryChange(gme_inventoryChanged);
		eng_observeObjectsChange(gme_objectsChanged);
		eng_observeActionChange(gme_actionChanged);
		eng_observeHintChange(gme_hintChanged);
		gme_updateNotification();
		_gme_opened = true;
	}
	_gme_setStatePicture(gameState);
	_gme_setMap(gameState);
}

function gme_close() {
	if (_gme_opened) {
		eng_unobserveLocationsChange(gme_locationsChanged);
		eng_unobserveActionsChange(gme_actionsChanged);
		eng_unobserveInventoryChange(gme_inventoryChanged);
		eng_unobserveObjectsChange(gme_objectsChanged);
		eng_unobserveActionChange(gme_actionChanged);
		eng_unobserveHintChange(gme_hintChanged);
		_gme_opened = false;
	}
}

// Result
/////////

/** Show a popup result with text. The title is automaticaly set
 * from the current action.
 * @param textResult The string code to be translated to show. */
function gme_showTextResult(textResult) {
	var title = null;
	if (act_getActionState() != ACT_PICK_ACTION) {
		// Get the title on regular command (shows the command itself)
		var actionPicked = act_getActionPicked();
		var actionCode = actionPicked['code'];
		var targetCount = act_getTargetsPickedCount();
		var i18n = "_default_action_completed_" + targetCount;
		var custI18n = "_" + actionCode + '_completed_' + targetCount;
		if (_gui_hasTranslation(custI18n, ENG_I18N_REQ_PRIORITY)) {
			i18n = custI18n;
		}
		var i18n_args = [];
		var linkers = [];
		// Get linkers, put defaults when undefined or null
		var linkers = [];
		for (var i = 0; i < targetCount; i++) {
			var custLinker = '_' + actionCode + '_completed_linker_' + i;
			if (_gui_hasTranslation(custLinker, ENG_I18N_REQ_PRIORITY)) {
				linkers.push(custLinker);
			} else {
				linkers.push('_default_action_completed_linker_' + i);
			}
		}
		// Build args action[/linker/target]*
		i18n_args.push(_gui_translate(actionPicked['code']));
		for (var i = 0; i < targetCount; i++) {
			i18n_args.push(linkers[i]);
			i18n_args.push(act_getTargetPicked(i));
		}
		title = _gui_translate(i18n, i18n_args);
	}
	var result = Array();
	if (Array.isArray(textResult)) {
		for (var i = 0; i < textResult.length; i++) {
			result.push(escHtml(_gui_translate(textResult[i])));
		}
	} else {
		result.push(escHtml(_gui_translate(textResult)));
	}
	gme_showResult(result, title, true);
}

/** Show result popup with message and optionnal title.
 * @param string The string code to be translated to show.
 * @param title (optional) The string code for the popup title. */
function gme_showResult(string, title, translated) {
	switch (arguments.length) {
	case 1:
		title = null;
	case 2:
		translated = false;
	}
	var result = Array();
	if (Array.isArray(string)) {
		for (var i = 0; i < string.length; i++) {
			if (translated) {
				result.push(escHtml(string[i]));
			} else {
				result.push(escHtml(_gui_translate(string[i])));
			}
		}
	} else {
		if (translated) {
			result.push(escHtml(string));
		} else {
			result.push(escHtml(_gui_translate(string)));
		}
	}
	var resultElem = document.getElementById('result');
	showElem(resultElem);
	if (title != null && !translated) {
		title = _gui_translate(title);
	}
	var html = Mustache.render(tpl_gameResult, {"title": title,
			"close": _gui_translate("_click_to_close")});
	resultElem.innerHTML = html;
	_gme_resultWriter = wrt_init(document.getElementById('result-text'), result);
	wrt_start(_gme_resultWriter);
}

/** Hide result popup */
function gme_closeResult() {
	if (_gme_resultWriter != null) {
		wrt_stop(_gme_resultWriter);
		_gme_resultWriter = null;
	}
	var resElem = document.getElementById('result');
	resElem.innerHTML = '';
	hideElem(resElem);
}

// Action
/////////

function gme_actionChanged() {
	gme_updateNotification();
}

/** Update notification area with current action state. */
function gme_updateNotification() {
	switch (act_getActionState()) {
	case ACT_PICK_ACTION:
		// No action picked
		document.querySelector('#notification div').innerHTML = _gui_translate("_pick_action");
		break;
	case ACT_PICK_TARGET:
		// Action picked, expecting targets
		var pickedCount = act_getTargetsPickedCount();
		var actionPicked = act_getActionPicked();
		var actionCode = actionPicked['code'];
		var i18n = "_default_action_builder_" + pickedCount;
		var i18n_args = [];
		var customI18nCode = '_' + actionCode + '_builder_' + pickedCount;
		if (_gui_hasTranslation(customI18nCode, ENG_I18N_REQ_PRIORITY)) {
			i18n = customI18nCode;
		}
		// Get linkers
		var linkers = [];
		for (var i = 0; i < (pickedCount + 1); i++) {
			var custLinker = '_' + actionCode + '_builder_linker_' + i
			if (_gui_hasTranslation(custLinker, ENG_I18N_REQ_PRIORITY)) {
				linkers.push(custLinker);
			} else {
				linkers.push('_default_action_builder_linker_' + i);
			}
		}
		// Build the array: action/linker[/target/linker]*
		i18n_args.push(_gui_translate(actionCode));
		i18n_args.push(_gui_translate(linkers[0]));
		for (var i = 0; i < pickedCount; i++) {
			i18n_args.push(_gui_translate(act_getTargetPicked(i)));
			i18n_args.push(_gui_translate(linkers[i + 1]));
		}
		html = _gui_translate(i18n, i18n_args);
		document.querySelector('#notification div').innerHTML = html;
		break;
	}
}

// Hints GUI
////////////

function gme_hintChanged(oldCount, newCount, hints) {
	if (newCount == oldCount + 1) {
		_gme_hints.push(hints[newCount - 1]);
		_gme_showNextHint(hints);
	} else {
		_gme_resetHints();
		for (var i = 0; i < newCount; i++) {
			_gme_hints.push(hints[i]);
			_gme_showNextHint(hints);
		}
	}
}

/** Remove all currently shown hints. */
function _gme_resetHints() {
	document.getElementById('hints').innerHTML = '';
	_gme_hintsShown = 0;
	_gme_hints = [];
}

/** Show next hint according to hints already shown. */
function _gme_showNextHint(hints) {
	var hintsElem = document.getElementById('hints');
	if (hints.length > _gme_hintsShown) {
		var elem = document.createElement('li');
		elem.setAttribute('onclick', 'javascript:gme_hintClicked(' + _gme_hintsShown + ');');
		elem.innerHTML = _gui_translate('_hint_{0}', [_gme_hintsShown + 1]);
		hintsElem.appendChild(elem);
	}
	_gme_hintsShown++;
}

function gme_hintClicked(hintIndex) {
	var hint = _gme_hints[hintIndex];
	var hintTitle = _gui_translate("_hint_{0}", [hintIndex + 1]);
	gme_showResult(hint, hintTitle);
}
// Picture and map
/////////////////////////

/** Show/update current state picture. */
function _gme_setStatePicture(state) {
	var escGameDir = escUrl(eng_getGameCode());
	var pictElem = document.getElementById('picture');
	pictElem.innerHTML = '';
	if ('picture' in state) {
		var imgElem = document.createElement('img');
		imgElem.setAttribute('src', './games/' + escGameDir + '/' + escUrl(state['picture']));
		// If big picture available, link to it on click
		if ('big_picture' in state) {
			var bigPictElem = document.createElement('a');
			bigPictElem.setAttribute('href', './games/' + escGameDir + '/' + escUrl(state['big_picture']));
			bigPictElem.setAttribute('target', '_blank');
			pictElem.appendChild(bigPictElem);
			bigPictElem.appendChild(imgElem);
		} else {
			pictElem.appendChild(imgElem);
		}
		showElem(pictElem);
	} else {
		// No picture available. Check for default or hide it.
		if (isDefined('default_picture')) {
			var imgElem = document.createElement('img');
			imgElem.setAttribute('src', './games/' + escGameDir + '/' + escUrl(default_picture));
			pictElem.appendChild(imgElem);
			showElem(pictElem);
		} else {
			hideElem(pictElem);
		}
	}
}

/** Show/update current state map. */
function _gme_setMap(state) {
	var escGameDir = escUrl(eng_getGameCode());
	var imgElem = document.querySelector('#map img');
	imgElem.setAttribute('src',  './games/' + escGameDir + '/' + escUrl(state['map']));
	var mapElem = document.querySelector('#map map');
	mapElem.innerHTML = '';
	for (var i = 0; i < state['map-items'].length; i++) {
		var item = state['map-items'][i];
		var areaElem = document.createElement('area');
		areaElem.setAttribute('coords', escAttr(item['area']));
		areaElem.setAttribute('shape', escAttr(item['shape']));
		areaElem.setAttribute('onclick', "javascript:eng_mapPicked(" + i + ");");
		mapElem.appendChild(areaElem);
	}
}

// Actions listing GUI
//////////////////////

_gme_actions = Array();

function gme_actionsChanged(actions) {
	_gme_actions = actions;
	_gme_setupActions(actions);
}

/** Show all available actions from the game and loaded languages. */
function _gme_setupActions(actions) {
	_gme_actions = actions;
	var actionsElem = document.querySelector('#actions ul');
	actionsElem.innerHTML = '';
	for (var i = 0; i < actions.length; i++) {
		var actionElem = document.createElement('li');
		actionElem.setAttribute('onclick', "javascript:eng_actionPicked('" + escJs(actions[i]['code']) + "');");
		actionElem.innerHTML = escHtml(_gui_translate(actions[i]['code']));
		actionsElem.appendChild(actionElem);
	}
}

// Inventory and items GUI
//////////////////////////

_gme_locations = Array();

function gme_locationsChanged(locations) {
	_gme_locations = locations;
	_gme_setupLocations(locations)
}
function gme_inventoryChanged(location, item) {
	_gme_setItem(location, item);
}
function gme_objectsChanged(operation, object) {
	switch (operation) {
	case ENG_OBJECT_ADD:
		_gme_addObject(object);
		break;
	case ENG_OBJECT_REM:
		_gme_removeObject(object);
		break;
	case ENG_OBJECT_SET:
		_gme_setObjects(object);
		break;
	case ENG_OBJECT_DISCOVERED:
		_gme_discoverObject(object);
		break;
	default:
		console.warn("Unknown object operation " + operation);
	}
}

/** Show all inventory locations from the game and loaded languages. */
function _gme_setupLocations(locations) {
	var inventoryElem = document.querySelector('#inventory ul');
	inventoryElem.innerHTML = '';
	for (var i = 0; i < locations.length; i++) {
		var locElem = document.createElement('li');
		locElem.setAttribute('id', 'location-' + escAttr(locations[i]));
		var nameElem = document.createElement('span');
		nameElem.setAttribute('class', 'location');
		nameElem.setAttribute('onclick', "javascript:eng_targetPicked('" + escJs(locations[i]) + "');");
		nameElem.innerHTML = escHtml(_gui_translate(locations[i]));
		itemElem = document.createElement('span');
		itemElem.setAttribute('class', 'item');
		itemElem.setAttribute('onclick', "javascript:eng_inventoryPicked('" + escJs(locations[i]) + "');");
		locElem.appendChild(nameElem);
		locElem.appendChild(itemElem);
		inventoryElem.appendChild(locElem);
	}
}

function _gme_setItem(location, item) {
	var itemElem = document.querySelector('#inventory ul li#location-' + escAttr(location) + ' .item');
	if (item == null) {
		itemElem.innerHTML = '';
	} else {
		itemElem.innerHTML = escHtml(_gui_translate(item));
	}
}

function _gme_addObject(item) {
	var itemElem = document.createElement('li');
	itemElem.setAttribute('id', 'object-' + escAttr(item));
	itemElem.setAttribute('onclick', 'javascript:eng_targetPicked("' + escJs(item) + '");');
	itemElem.innerHTML = escHtml(_gui_translate(item));
	document.querySelector('#objects ul').appendChild(itemElem);
}

function _gme_removeObject(item) {
	var itemElem = document.getElementById('object-' + escAttr(item));
	itemElem.parentNode.removeChild(itemElem);
}

function _gme_setObjects(items) {
	document.querySelector('#objects ul').innerHTML = '';
	for (var i = 0; i < items.length; i++) {
		_gme_addObject(items[i]);
	}
}

function _gme_discoverObject(item) {
	// Do nothing
}