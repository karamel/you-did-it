/* You DID it!
 * 
 * This file is part of You DID it!
 * 
 * You DID it! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You DID it! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * You DID it! uses jQuery, licensed by the jQuery Fundation
 * under the MIT License (see https://jquery.org/license/)
 * and Mustache.js, licensed under the MIT License.
 */
 
// This file contains internationalization (i18n) functions
// Prefix: i18

/** Create an empty translation object. */
function i18n_init() {
	return {'loaded': [], 'data': {}, 'priority': {}};
}

/** Translate a text
 * @param string The string code to translate
 * @param array An optional array for translation placeholders
 */
function i18_translate(i18nData, string, array) {
	if (string in i18nData['data']) {
		if (Array.isArray(array)) {
			for (var i = 0; i < array.length; i++) {
				array[i] = i18_translate(i18nData, array[i]);
			}
			return i18nData['data'][string].format(array);
		} else {
			return i18nData['data'][string];
		}
	}
	return string;
}

/** Check if a translation is available.
 * @param i18n The translation data to look into.
 * @param string The key to look translation for.
 * @param priority (optional) The minimum priority requested.
 */
function i18_hasTranslation(i18nData, key, priority) {
	switch(arguments.length) {
	case 2: priority = null;
	}
	if (priority !== null) {
		return (key in i18nData['data'] && i18nData['priority'][key] >= priority);
	} else {
		return (key in i18nData['data']);
	}
}

/** Load an language file and merge its data into the translations dict.
 * Higher priority values are kept, while lower are overriden.
 * @param i18nData (i18n data object) the i18n data to expand.
 * @param language (string) the language code to be loaded.
 * @param translations (associative array of string)
 * the key => translation data.
 * @param priority (int) the priority of the given language. */
function i18_loadLanguage(i18nData, language, translations, priority) {
	for (var key in translations) {
		if (key in i18nData['priority']) {
			if (i18nData['priority'][key] < priority) {
				// Override previous data with less priority
				i18nData['data'][key] = translations[key];
				i18nData['priority'][key] = priority;
			}
		} else {
			// No previous data for this key
			i18nData['data'][key] = translations[key];
			i18nData['priority'][key] = priority;
		}
	}
	i18nData['loaded'].push(language);
}